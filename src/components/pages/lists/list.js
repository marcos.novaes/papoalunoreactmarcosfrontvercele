import React, { useState } from 'react'
import './lists.css'


const List = ({ lista, index, removerLista, editarLista }) => {

    const [list] = useState(lista)
    const [items, setItems] = useState(list.items)

    const handleChange = (e, i) => {
        let itemArray = items
        itemArray[i].feito = itemArray[i].feito ? false : true
        setItems(itemArray)

    }

    const handleDelete = () => {
        try {
            const result = window.confirm("tem certeza que quer apagar?")
            if (result) {
                removerLista(list._id)
            }
        } catch (error) {
            alert(error.message)
        }

    }

    const botaoUrgencia = (item) => {
        if (item.urgente) {
            return (
                <button className="botaoUrgente">urgente</button>
            )
        }
        else {
            return (
                <button className="botaoNUrgente">não urgente</button>
            )
        }
    }

    const alteraLista = () => {
        try {
            let body = {
                ...list
            }
            body["items"] = items
            editarLista(body, list._id)
        } catch (error) {
            alert('deu ruim')
        }
    }



    const itemFeito = (item) => {
        return item.feito ? "itemFeito" : ""
    }

    const montaItems = (items) => {
        return (
            items.map((item, i) => (
                <li className={itemFeito(item)} key={i}>
                    {item.descricao + ' | '} {botaoUrgencia(item)}
                    <input onChange={(e) => handleChange(e, i)}
                        type="checkbox"
                        name="feito"
                        value="true"
                        checked={item.feito}>
                    </input>
                </li>
            ))
        )
    }

    const montaLista = () => {
        return (
            <div className="lista">
                <h2>{list.nome}<button onClick={() => handleDelete()} className="botaoApagar">Apagar</button></h2>

                <ul>
                    {montaItems(list.items)}
                </ul>
                <button onClick={() => alteraLista()}>Salvar alteraçoes!</button>
            </div>
        )

    }

    return montaLista()
}

export default List