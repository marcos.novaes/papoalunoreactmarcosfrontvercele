import React from 'react';
import './App.css';
import Header from './components/layout/header/header'
import Footer from './components/layout/footer/footer'
// import Login from './components/pages/login/login'
import Lists from './components/pages/lists/lists'

function App() {
  return (
    <React.Fragment>
      <Header></Header>
      <div className="body">
        <Lists></Lists>
      </div>
      <Footer></Footer>
    </React.Fragment>
  );
}

export default App;
