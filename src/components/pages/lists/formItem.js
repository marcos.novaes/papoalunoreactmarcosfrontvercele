import React, { useState } from 'react'
import './lists.css'

const FormItem = ({ incluirTarefa }) => {
    const [item, setItem] = useState({
        "urgente": "false",
        "feito": "false"
    })

    const handleChange = (e) => {
        setItem({
            ...item,
            [e.target.name]: e.target.value
        })
        return
    }

    const add = () => {
        incluirTarefa(item)
        setItem({
            "urgente": "false",
            "feito": "false"
        })
    }

    return (
        <div className="form-group">
            <label htmlFor="item">Tarefas:</label>
            <input onChange={(e) => handleChange(e)} value={item.descricao || ""} name="descricao" id="inputItem" />
            <select onChange={(e) => handleChange(e)} name="urgente" id="inputUrgencia">
                <option value="false">Não urgente</option>
                <option value="true">Urgente</option>
            </select>
            <button onClick={() => add()}>+</button>
        </div>
    )
}

export default FormItem

